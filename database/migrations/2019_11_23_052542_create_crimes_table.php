<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crimes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dr_no');
            $table->dateTime('date_reported');
            $table->date('date_occ');
            $table->string('time_occ');
            $table->integer('area');
            $table->string('rpt_dist_no');
            $table->integer("part_1_2");
            $table->integer('crime_code');
            $table->integer('victim_age');
            $table-integer('victim_sex_code');
            $table->integer('victim_descent_code');
            $table->integer('premise_code');
            $table->integer('weapon_used_code');
            $table->integer('status_code');
            $table->integer('crime_code_1');
            $table->integer('crime_code_2');
            $table->integer('crime_code_3');
            $table->integer('crime_code_4');
            $table->string('location');
            $table->string('cross_street');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crimes');
    }
}
