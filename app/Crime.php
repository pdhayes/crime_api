<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crime extends Model
{
    //
    public function crimeArea(){
        return $this->hasOne('App\Area');
    }

    public function modusOperandi(){
        return $this->hasMany('App\ModusOperandi');
    }

    public function premiseCode(){
        return $this->hasOne('App\Premise');
    }

    public function weapon(){
        return $this->hasOne('App\WeaponUsed');
    }

    public function crimeStatus(){
        return $this->hasOne('App\Status');
    }

    public function victimDescent(){
        return $this->hasOne('App\VictimDescent');
    }

    public function victimSex(){
        return $this->hasOne('App\VictimSex');
    }
}
