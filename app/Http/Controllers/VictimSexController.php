<?php

namespace App\Http\Controllers;

use App\VictimSex;
use Illuminate\Http\Request;

class VictimSexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VictimSex  $victimSex
     * @return \Illuminate\Http\Response
     */
    public function show(VictimSex $victimSex)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VictimSex  $victimSex
     * @return \Illuminate\Http\Response
     */
    public function edit(VictimSex $victimSex)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VictimSex  $victimSex
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VictimSex $victimSex)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VictimSex  $victimSex
     * @return \Illuminate\Http\Response
     */
    public function destroy(VictimSex $victimSex)
    {
        //
    }
}
