<?php

namespace App\Http\Controllers;

use App\CrimeModusOperandi;
use Illuminate\Http\Request;

class CrimeModusOperandiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CrimeModusOperandi  $crimeModusOperandi
     * @return \Illuminate\Http\Response
     */
    public function show(CrimeModusOperandi $crimeModusOperandi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CrimeModusOperandi  $crimeModusOperandi
     * @return \Illuminate\Http\Response
     */
    public function edit(CrimeModusOperandi $crimeModusOperandi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CrimeModusOperandi  $crimeModusOperandi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CrimeModusOperandi $crimeModusOperandi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CrimeModusOperandi  $crimeModusOperandi
     * @return \Illuminate\Http\Response
     */
    public function destroy(CrimeModusOperandi $crimeModusOperandi)
    {
        //
    }
}
