<?php

namespace App\Http\Controllers;

use App\WeaponUsed;
use Illuminate\Http\Request;

class WeaponUsedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeaponUsed  $weaponUsed
     * @return \Illuminate\Http\Response
     */
    public function show(WeaponUsed $weaponUsed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeaponUsed  $weaponUsed
     * @return \Illuminate\Http\Response
     */
    public function edit(WeaponUsed $weaponUsed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeaponUsed  $weaponUsed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeaponUsed $weaponUsed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeaponUsed  $weaponUsed
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeaponUsed $weaponUsed)
    {
        //
    }
}
