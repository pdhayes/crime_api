<?php

namespace App\Http\Controllers;

use App\VictimDescent;
use Illuminate\Http\Request;

class VictimDescentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VictimDescent  $victimDescent
     * @return \Illuminate\Http\Response
     */
    public function show(VictimDescent $victimDescent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VictimDescent  $victimDescent
     * @return \Illuminate\Http\Response
     */
    public function edit(VictimDescent $victimDescent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VictimDescent  $victimDescent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VictimDescent $victimDescent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VictimDescent  $victimDescent
     * @return \Illuminate\Http\Response
     */
    public function destroy(VictimDescent $victimDescent)
    {
        //
    }
}
