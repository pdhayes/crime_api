<?php

namespace App\Http\Controllers;

use App\CrimeCode;
use Illuminate\Http\Request;

class CrimeCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CrimeCode  $crimeCode
     * @return \Illuminate\Http\Response
     */
    public function show(CrimeCode $crimeCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CrimeCode  $crimeCode
     * @return \Illuminate\Http\Response
     */
    public function edit(CrimeCode $crimeCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CrimeCode  $crimeCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CrimeCode $crimeCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CrimeCode  $crimeCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(CrimeCode $crimeCode)
    {
        //
    }
}
