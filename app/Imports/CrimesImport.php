<?php

namespace App\Imports;

use App\Crime;
use App\Area;
use App\CrimeCode;
use App\CrimeModusOperandi;
use App\ModusOperandi;
use App\Premise;
use App\Status;
use App\VictimDescent;
use App\VictimSex;
use App\WeaponUsed;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CrimesImport implements ToModel, WithChunkReading, WithHeadingRow
{
    public $data = [];
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $this->data[] = $row;
        /*return new Crime([
            //
        ]);*/
    }

    public function chunkSize(): int
    {
        return 20000;
    }

    public function headingRow(): int
    {
        return 2;
    }

    public function process(){


    }
}
